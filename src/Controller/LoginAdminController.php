<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginAdminController extends Controller
{
    /**
     * @Route("/login/admin", name="login_admin")
     */
    public function index(AuthenticationUtils $utils)
    {

        $error = $utils->getLastAuthenticationError();
        $lastEmail = $utils->getLastUsername();

        return $this->render('login_admin/index.html.twig', [
            'controller_name' => 'LoginAdminController', 'error' => $error, 'lastEmail' => $lastEmail
        ]);
    }
}
