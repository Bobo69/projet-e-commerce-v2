<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\AccountType;
use Symfony\Component\HttpFoundation\Request;

class AccountController extends Controller
{
    /**
     * @Route("/account", name="account")
     */
    public function index(Request $req/*, UserPasswordEncoderInterface $encoder*/)
    {
        $form = $this->createForm(AccountType::class);
        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            // $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        return $this->render('account/index.html.twig', [
            'controller_name' => 'AccountController', 'formAccount' => $form->createView()
        ]);
    }
    // public function register(UserPasswordEncoderInterface $encoder)
    // {
    // // whatever *your* User object is
    //     $user = new App\Entity\User();
    //     $plainPassword = 'ryanpass';
    //     $encoded = $encoder->encodePassword($user, $plainPassword);

    //     $user->setPassword($encoded);
    // }
}
