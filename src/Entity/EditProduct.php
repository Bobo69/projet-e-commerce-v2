<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class EditProduct
{
    private $id;
    /**
     * @Assert\NotBlank(message="Please, upload the editProduct file as a jpeg file.")
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $file;

    public function getId()
    {
        return $this->id;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file): self
    {
        $this->file = $file;

        return $this;
    }
}
